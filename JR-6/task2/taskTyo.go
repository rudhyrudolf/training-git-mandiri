package main

import "fmt"

func main() {

	bunga := map[string]int{
		"awal":  11,
		"akhir": 8,
	}

	fmt.Print("Masukkan jumlah hutang : ")
	var hutang int
	fmt.Scanln(&hutang)

	fmt.Print("Masukkan banyaknya angsuran : ")
	var cicilan int
	fmt.Scanln(&cicilan)

	var bayar int

	fmt.Println("====================================================")
	fmt.Println("Jumlah hutang sebesar : ", hutang)
	fmt.Println("Diangsur sebanyak : ", cicilan)
	fmt.Println("====================================================")
	var half_cicil = cicilan / 2

	for i := 1; i <= cicilan; i++ {
		if i <= half_cicil {
			bayar = (hutang / cicilan) + (hutang * bunga["awal"] / 100)
			fmt.Println("Angsuran bulan ", i, "bunga (11%) sebesar", bayar)
		} else {
			bayar = (hutang / cicilan) + (hutang * bunga["akhir"] / 100)
			fmt.Println("Angsuran bulan ", i, "bunga (8%) sebesar", bayar)
		}
	}

}
