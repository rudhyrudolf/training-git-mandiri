package task2

import "fmt"

func main() {
	var hutang, durasi, angsuran int
	var bunga = map[string]int{"bungaA": 11, "bungaB": 8}

	fmt.Println("Simulasi Pinjaman Dengan Suku Bunga Sebagai Berikut ")
	fmt.Print("Suku Bunga '11%' dan '8%' ")
	fmt.Println("Jika angsuran 12 maka 6 bulan pertama '11%' dan selanjutnya '8%' ")
	fmt.Print("Masukkan Jumlah Pinjaman = ")
	fmt.Scan(&hutang)
	fmt.Print("Input Lama Angsuran (dalam bulan) = ")
	fmt.Scan(&durasi)

	fmt.Println("----------------------------------------------------------------------------------------")
	fmt.Print("")
	fmt.Println("Anda akan meminjam uang sebesar = ", hutang)
	fmt.Println("Selama = ", durasi, " bulan")
	fmt.Println("----------------------------------------------------------------------------------------")

	var cicil = durasi / 2
	for i := 1; i <= durasi; i++ {
		if i <= cicil {
			angsuran = (hutang / durasi) + (hutang * bunga["bungaA"] / 100)
			fmt.Println("Angsuran bulan ke-", i, "bunga (11%) sebesar", angsuran)
		} else {
			angsuran = (hutang / durasi) + (hutang * bunga["bungaB"] / 100)
			fmt.Println("Angsuran bulan ke-", i, "bunga 8% sebesar", angsuran)
		}
	}
}
