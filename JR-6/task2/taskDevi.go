//package task2
package main

import "fmt"

func main() {
	var pinjaman int
	var angsuran int
	var cicil int

	bunga := map[string]int{
		"awal":  11,
		"akhir": 8,
	}

	fmt.Println("*** Tugas 2 Devi ***")
	fmt.Println("Simulasi Pinjaman dengan Suku Bunga sebagai berikut")
	fmt.Println("Suku Bunga '11%' dan '8%' ")
	fmt.Println("Jika angsuran 12 maka 6 bulan pertama '11%' dan selanjutnya '8%' ")

	fmt.Printf(" Masukkan Jumlah Pinjaman = ")
	fmt.Scan(&pinjaman)
	fmt.Printf(" Masukkan Lama Angsuran dlm bulan = ")
	fmt.Scan(&angsuran)
	fmt.Println("\n--------------------------------------------")
	fmt.Println("Anda Akan Meminjam Uang Sebesar = ", pinjaman)
	fmt.Printf("Selama = %d bulan", angsuran)
	fmt.Println("\n--------------------------------------------")

	for i := 1; i <= angsuran; i++ {
		if i <= angsuran/2 {
			cicil = ((pinjaman * bunga["awal"] / 100) / angsuran)
			fmt.Printf("\n Angsuran %d bunga ( ", i)
			fmt.Print(bunga["awal"])
			fmt.Print("% ) : ", cicil)
		}

		if i > angsuran/2 {
			cicil = (pinjaman / angsuran * bunga["akhir"] / 100)
			fmt.Printf("\n Angsuran %d bunga ( ", i)
			fmt.Print(bunga["akhir"])
			fmt.Print("% ) : ", cicil)
		}
	}

}
