package materi

import "fmt"

func Materi2() {
	var names [5]string

	names[0] = "Rudi"
	names[1] = "Ilham"
	names[2] = "tyo"
	names[3] = "devi"
	names[4] = "tes"

	var names2 [2]string
	names2 = [2]string{"rudi hermawan", "ilham tyo"}

	fmt.Println(names)
	fmt.Println(names2)

	var number = [...]int{1, 2, 3, 4, 5, 6}
	fmt.Println("data array \t", number)
	fmt.Println("jumlah elemen array \t", len(number))

	// array multidimensi

	var number1 = [2][3]int{[3]int{1, 2, 3}, [3]int{4, 5, 6}}

	var number2 = [2][3]int{{2, 3, 3}, {3, 4, 4}}

	fmt.Println(number1)
	fmt.Println(number2)

	for i := 0; i < len(names); i++ {
		fmt.Printf("element %d : %s \n", i, names[i])
	}

	for _, names := range names {

		fmt.Printf("nama : %s \n", names)

	}

	var names3 = make([]string, 2)
	names3[0] = "Belajar"
	names3[1] = "golang"

	fmt.Println(names3)

	// slice
	var fruit = []string{"apple", "grape", "banana", "melon"}
	fmt.Println(fruit[0])

	// var fruitA = []string{"manggo","banana"} // slice
	// var fruitB = [2]string{"aple","grape"} // array
	var fruittC = []string{"papaya", "grape", "melon", "aple"} // array

	var newFuits = fruittC[0:3]
	var bFruits = fruittC[1:4]

	var aafruits = fruittC[1:2]
	var bbfruits = fruittC[0:1]

	fmt.Println(newFuits)
	fmt.Println(bFruits)

	fmt.Println(aafruits)
	fmt.Println(bbfruits)

	//
	bbfruits[0] = "pinnaple"

	fmt.Println(fruittC)
	fmt.Println(newFuits)
	fmt.Println(bFruits)
	fmt.Println(aafruits)
	fmt.Println(bbfruits)

	fmt.Println(len(newFuits))
	fmt.Println(cap(newFuits))
	newFuits = append(newFuits, "Manggo")

	fmt.Println(cap(newFuits))

	dst := make([]string, 3)

	dst = []string{"banana", "cocolate"}

	src := []string{"watermelon", "pinnaple", "apple", "orange"}

	n := copy(dst, src)

	fmt.Println(dst)
	fmt.Println(src)
	fmt.Println(n)

	// map

	var month = map[string]int{
		"january": 10,
		"febuary": 20,
	}

	// month = map[string]int{}

	// month["january"] = 50
	// month["january"] = 40

	fmt.Println("january", month["january"])

	// var chicken3 = map[string]int{}
	// var chicken4 = make(map[string]int)
	// var chicken5 = *new(map[string]int)
}
